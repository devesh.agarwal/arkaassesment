import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ErrorPage from './ErrorPage/ErrorPga';
import SubmissionPage from './SubmissionPage/SubmissionPage';
import UserForm from './UserForm/UserForm';

function RouteFunction() {

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<UserForm />}></Route>
                <Route path="/contact-form" element={<UserForm />}></Route>
                <Route path="/submission-successful" element={<SubmissionPage />}></Route>
                <Route path="*" element={<ErrorPage />}></Route>
            </Routes>
        </BrowserRouter>

    )
}

export default RouteFunction;
