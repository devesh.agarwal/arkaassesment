import './SubmissionPage.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Fragment, useState } from 'react';
import DataHolding from '../service';

function SubmissionPage() {
    const [userData] = useState(DataHolding.getUserData())

    return (
        <Fragment>
      
            <div className='table-responsive submissionPage' >
                <h1>Your form has been submitted successfully.</h1>
                <table className="table table-hover">
                    <caption >Details </caption>
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Password</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        < tr >
                            <td  >{userData.name}</td>
                            <td>{userData.email}</td>
                            <td>{userData.password}</td>
                            <td>{userData.phone}</td>
                            <td>{userData.address}</td>
                        </tr >
                    </tbody>
                </table>
            </div>
        </Fragment>)
}


export default SubmissionPage