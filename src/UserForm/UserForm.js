import './UserForm.css';
import * as Yup from "yup";
import { Formik } from 'formik';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Fragment } from 'react';
import background from "../images/background.jpg";
import DataHolding from '../service';
import { useNavigate } from "react-router-dom";

const formDataError = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .required('Required'),
    email: Yup.string()
        .required('Required')
        .email('Enter Correct Email address'),
    password: Yup.string()
        .required('Required')
        .matches(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9]).{8,}$/, "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"),
    phone: Yup.number()
        .required('Required')
        .integer('Invalid Number')
        .test('len', 'Must be a vaild Moblie number', val => val && val.toString().length === 10),
    address: Yup.string()
        .required('Required')

});

function UserForm() {
   
    const history = useNavigate();
        const element =
        <Formik initialValues={{ name: '', email: '', password: '', phone: '', address: '' }}
            validationSchema={formDataError}
            onSubmit={(values) => {
                    DataHolding.setUserData(values);
                let path = '/submission-successful';
                history(path);
            }} >

            {({ values, errors, touched, handleChange, handleBlur, handleSubmit }) => (
                <form className='userForm' onSubmit={handleSubmit} autoComplete="off">

                    <div className="formRow col-10 col-sm-4 ">
                        <input type="text" name="name" placeholder='name' className=" form-control  " onChange={handleChange} onBlur={handleBlur} value={values.name} />
                        {errors.name && touched.name && <div className='valdationColor'>{errors.name}*</div>}
                    </div>

                    <div className="formRow col-10 col-sm-4 ">
                        <input type="email" name="email" placeholder='email' className="form-control  " onChange={handleChange} onBlur={handleBlur} value={values.email} />
                        {errors.email && touched.email && <div className='valdationColor'>{errors.email}*</div>}
                    </div>

                    <div className="formRow col-10 col-sm-4 ">
                        <input type="text" name="password" placeholder='password' className=" form-control  " onChange={handleChange} onBlur={handleBlur} value={values.password} />
                        {errors.password && touched.password && <div className='valdationColor'>{errors.password}*</div>}
                    </div>

                    <div className="formRow col-10 col-sm-4 ">
                        <input type="number" name="phone" placeholder='phone' className="form-control  " onChange={handleChange} onBlur={handleBlur} value={values.phone} />
                        {errors.phone && touched.phone && <div className='valdationColor'>{errors.phone}*</div>}
                    </div>

                    <div className="formRow col-10 col-sm-4 ">
                        <input type="text" name="address" className="form-control " placeholder='address' onChange={handleChange} onBlur={handleBlur} value={values.address} />
                        {errors.address && touched.address && <div className='valdationColor'>{errors.address}*</div>}
                    </div>

                    <button className="loginButton btn btn-outline-light " type="submit">login</button>
                </form>)}
        </Formik>

    return (
        <Fragment>
            <div className="fill-window" style={{ backgroundImage: `url(${background})`}}>
            <p>Welcome</p>
            {element}
            </div>
        </Fragment>
    )
};

export default UserForm;


