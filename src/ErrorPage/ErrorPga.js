import './ErrorPage.css';
import { Fragment } from "react"

function ErrorPage() {
    return (
        <Fragment>
            <div>
                <p className='errorMessage'>
                    Something Went Wrong.
                    <br/> Please try again.
                </p>
            </div>
        </Fragment>
    )
}

export default ErrorPage