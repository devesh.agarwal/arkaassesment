import './App.css';
import RouteFunction from './Router';
import { Fragment } from 'react';

function App() {
    return (
        <Fragment>
            <RouteFunction />
        </Fragment>
    );
}

export default App;
